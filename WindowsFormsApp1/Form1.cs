﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        bool red = true; //kad je true- igra X, kad je false igra Y
        int brojac_poteza = 0;
        static String igrac1, igrac2;

        public static void postavi_imena(string n1, string n2)
        {
            igrac1 = n1;
            igrac2 = n2;
        }

        public Form1()
        {
            InitializeComponent();
            
           
        }


        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {

        }

        private void opisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("iks-oks za 2 igrača.", "Pozdrav");
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (red) { 
                b.Text = "X";
                label4.Text = igrac2 + " je na potezu!";
                
            }
            else 
            { 
                b.Text = "O";
                label4.Text = igrac1 + " je na potezu!";
               
            }
            red = !red;
            b.Enabled = false;
            brojac_poteza++;
            provjeri_pobjednika();
          
        }

        private void provjeri_pobjednika() 
        { 
            bool ima_pobjednik = false;
            //vodoravna provjera
            if ((A1.Text == A2.Text) && (A2.Text == A3.Text)&&(!A1.Enabled)) 
                 {  ima_pobjednik = true; }
            else if ((B1.Text == B2.Text) && (B2.Text == B3.Text)&&(!B1.Enabled))
                 { ima_pobjednik = true; }
            else if ((C1.Text == C2.Text) && (C2.Text == C3.Text) && (!C1.Enabled))
                 { ima_pobjednik = true; }
            //okomita provjera
            else if ((A1.Text == B1.Text) && (B1.Text == C1.Text) && (!A1.Enabled))
            { ima_pobjednik = true; }
            else if ((A2.Text == B2.Text) && (B2.Text == C2.Text) && (!A2.Enabled))
            { ima_pobjednik = true; }
            else if ((A3.Text == B3.Text) && (B3.Text == C3.Text) && (!A3.Enabled))
            { ima_pobjednik = true; }
            //dijagonalna provjera
            else if ((A1.Text == B2.Text) && (B2.Text == C3.Text) && (!A1.Enabled))
            { ima_pobjednik = true; }
            else if ((A3.Text == B2.Text) && (B2.Text == C1.Text) && (!A3.Enabled))
            { ima_pobjednik = true; }


            if (ima_pobjednik)
            {
                disable_buttons();
                String pobjednik = "";
                if (red)
                {
                    pobjednik = igrac2;
                    O_pobjede.Text = (Int32.Parse(O_pobjede.Text) + 1).ToString();
                
                }
                else
                {
                    pobjednik = igrac1;
                    X_pobjede.Text = (Int32.Parse(X_pobjede.Text) + 1).ToString();
                }
                MessageBox.Show(pobjednik + " je pobjedio!", "Rezultat");
            }//end if
            else 
            {
                if (brojac_poteza == 9)
                {
                    Izjednaceno.Text = (Int32.Parse(Izjednaceno.Text) + 1).ToString();
                    MessageBox.Show("Izjednaceno!", "Rezultat");
                }
            }
  
        } // end provjeri_pobjedinka

        private void disable_buttons() 
        {

            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                }
                catch { }
            }
           

        }

        private void novaIgraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            red = true;
            brojac_poteza = 0;

            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";
                }

                catch { }
            }
        }

        private void button_enter(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                if (red)
                {
                    b.Text = "X";
                }
                else b.Text = "O";
            }

        }

        private void button_leave(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                b.Text = "";
            }
            
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            X_pobjede.Text = "0";
            O_pobjede.Text = "0";
            Izjednaceno.Text = "0";

            red = true;
            brojac_poteza = 0;

            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";
                }

                catch { }
            }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Form2 f2 = new Form2();
            f2.ShowDialog();
            label4.Text = igrac1 + " je na potezu!";
            label1.Text = igrac1;
            label3.Text = igrac2;
            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.BackColor = Color.Green;
                }

                catch { }
            }
            

        }

       
       


    }
}
