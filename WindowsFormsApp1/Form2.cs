﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void kreni_Click(object sender, EventArgs e)
        {
            Form1.postavi_imena(igrac1.Text, igrac2.Text);
            this.Close();

        }

        private void igrac2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString() == "\r")
                kreni.PerformClick();
        }
    }
}
